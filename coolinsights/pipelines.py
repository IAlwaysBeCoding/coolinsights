# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html


import csv

from coolinsights.items import Main, CEP, Reference, Practical

class CoolinsightsPipeline(object):


    def __init__(self):

        self.cep_file = open('cep.csv','w+')
        self.reference_file = open('reference.csv','w+')
        self.practical_file = open('practical.csv','w+')
        self.main_file = open('main.csv','w+')

        self.cep_writer = csv.DictWriter(self.cep_file, delimiter='|', fieldnames=list(CEP.fields.keys()))
        self.reference_writer = csv.DictWriter(self.reference_file, delimiter='|', fieldnames=list(Reference.fields.keys()))
        self.practical_writer = csv.DictWriter(self.practical_file, delimiter='|', fieldnames=list(Practical.fields.keys()))
        self.main_writer = csv.DictWriter(self.main_file, delimiter='|', fieldnames=list(Main.fields.keys()))

        self.cep_writer.writeheader()
        self.reference_writer.writeheader()
        self.practical_writer.writeheader()
        self.main_writer.writeheader()


    def close_spider(self, spider):
        self.cep_file.close()
        self.reference_file.close()
        self.practical_file.close()
        self.main_file.close()

    def process_item(self, item, spider):

        if isinstance(item, CEP):
            writer = self.cep_writer
        elif isinstance(item, Reference):

            writer = self.reference_writer
        elif isinstance(item, Practical):

            writer = self.practical_writer
        elif isinstance(item, Main):
            writer = self.main_writer

        writer.writerow(item)
        return item
