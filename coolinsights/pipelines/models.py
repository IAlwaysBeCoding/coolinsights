from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy import Column, ForeignKey, Sequence, Integer, \
                        String, Text, Boolean, DateTime, create_engine

from sqlalchemy.orm import scoped_session, sessionmaker

from scrapy.utils.project import get_project_settings

SETTINGS = get_project_settings()
DATABASE = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(
        SETTINGS['MYSQL_USERNAME'], SETTINGS['MYSQL_PASSWORD'],
        SETTINGS['MYSQL_HOST'], SETTINGS['MYSQL_PORT'] , SETTINGS['MYSQL_DATABASE'])

Base = declarative_base()
engine = create_engine(DATABASE, convert_unicode=True, echo=False)


class DatabaseException(Exception):
    pass

class EdqmCEPDb(Base):
    __tablename__ = "EdqmCEP"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    SubstanceNumber = Column(String(128))
    Substance = Column(String(128))
    CertificateHolder = Column(String(128))
    CertificateNumber = Column(String(128))
    IssueDate = Column(String(128))
    Status = Column(String(128))
    Enddate	= Column(String(128))
    Type = Column(String(128))

class EdqmReferenceDb(Base):
    __tablename__ = "EdqmReference"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    Availablesince = Column(String(128))
    CatNo = Column(String(128))
    Name = Column(String(128))
    BatchNo = Column(String(128))
    UnitQuantity = Column(String(128))
    Price = Column(String(128))
    SDSProductCode = Column(String(128))
    MonographNumber = Column(String(128))

class EdqmPracticalDb(Base):
    __tablename__ = "EdqmPractical"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    Tests = Column(String(128))
    BrandNameInformation = Column(String(128))
    MonographNumber = Column(String(128))

class EdqmMainDb(Base):
    __tablename__ = "EdqmMain"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    Status = Column(String(128))
    MonographNumber = Column(String(128))
    EnglishName = Column(String(128))
    FrenchName = Column(String(128))
    LatinName = Column(String(128))
    PinyinName = Column(String(128))
    ChineseName = Column(String(128))
    Pharmeuropa = Column(String(128))
    PublishedinEnglishSupplement = Column(String(128))
    PublishedinFrenchSupplement = Column(String(128))
    Ongoing = Column(String(128))
    Stateofwork = Column(String(128))
    Pharmeuropa = Column(String(128))
    Description = Column(String(128))
    Chromatogram = Column(String(128))
    Additionalinformation = Column(String(128))
    History = Column(String(128))
    InterchangeableICH_Q4B = Column(String(128))
    InternationalHarmonisationchapter = Column(String(128))

class MedsafeCompositionDb(Base):
    __tablename__ = "MedsafeComposition"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    MedID = Column(String(128))
    Ingredient = Column(String(128))
    Manufacturer = Column(String(128))
    Component = Column(String(128))

class MedsafeProductionDb(Base):
    __tablename__ = "MedsafeProduction"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    MedID = Column(String(128))
    Manufacturer = Column(String(128))
    Manufacturingstep = Column(String(128))

class MedsafePackagingDb(Base):
    __tablename__ = "MedsafePackaging"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    MedID = Column(String(128))
    Contents = Column(String(128))
    ShelfLife = Column(String(128))
    Package = Column(String(128))

class MedsafeLatestRegulatoryActivityDb(Base):
    __tablename__ = "MedsafeLatestRegulatoryActivity"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    MedID = Column(String(128))
    PaymentDate = Column(String(128))
    Changes = Column(String(128))
    Status = Column(String(128))
    Priority = Column(String(128))
    ApplicationType = Column(String(128))
    ApplicationDate = Column(String(128))

class MedsafeIndicationsDb(Base):
    __tablename__ = "MedsafeIndications"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    MedID = Column(String(128))
    text = Column(String(128))

class MedsafeMainDb(Base):
    __tablename__ = "MedsafeMain"

    Id = Column(Integer, autoincrement=True, primary_key=True)
    MedID = Column(String(128))
    TradeName = Column(String(128))
    DoseForm = Column(String(128))
    Strength = Column(String(128))
    Identifier= Column(String(128))
    Sponsor	= Column(String(128))
    Applicationdate	= Column(String(128))
    Registrationsituation= Column(String(128))
    Classification = Column(String(128))


Base.metadata.create_all(engine)

DBSession = scoped_session(sessionmaker())
DBSession.configure(bind=engine, autoflush=False, expire_on_commit=False)

