
from datetime import timedelta, datetime
import time

from scrapy.utils.project import get_project_settings
from scrapy.signals import engine_stopped

from coolinsights.items import (EdqmCEP, EdqmReference, EdqmPractical, EdqmMain,
                                MedsafeComposition, MedsafeProduction, MedsafePackaging,
                                MedsafeLatestRegulatoryActivity, MedsafeIndications, MedsafeMain)
from coolinsights.pipelines.models import *


class DatabaseExporter(object):

    def process_item(self, item, spider):

        if isinstance(item, (EdqmCEP, EdqmReference, EdqmPractical, EdqmMain)):
            self.insert_edqm_item(item)

        elif isinstance(item, (MedsafeComposition, MedsafeProduction, MedsafePackaging,
                                MedsafeLatestRegulatoryActivity, MedsafeIndications, MedsafeMain)):

            self.insert_medsafe_item(item)

        else:
            spider.log('wtf:{}'.format(type(item)))



        return item

    @classmethod
    def from_crawler(cls, crawler):

        crawler.signals.connect(cls.close_db_session, signal=engine_stopped)

        return cls()

    @staticmethod
    def close_db_session():
        DBSession.close()

    def insert_item(self,item, db_class):
        db_item = db_class(**item)
        print(vars(db_item))
        DBSession.add(db_item)

        try:
            DBSession.commit()
        except Exception as e:
            print(e)
            DBSession.rollback()
            raise DatabaseException(e)

    def insert_edqm_item(self, item):

        if isinstance(item, EdqmCEP):
            T = EdqmCEPDb
        elif isinstance(item, EdqmReference):
            T = EdqmReferenceDb
        elif isinstance(item, EdqmPractical):
            T = EdqmPracticalDb
        elif isinstance(item, EdqmMain):
            T = EdqmMainDb

        self.insert_item(item, T)

    def insert_medsafe_item(self, item):

        if isinstance(item, MedsafeComposition):
            T = MedsafeCompositionDb
        elif isinstance(item, MedsafeProduction):
            T = MedsafeProductionDb
        elif isinstance(item, MedsafePackaging):
            T = MedsafePackagingDb
        elif isinstance(item, MedsafeLatestRegulatoryActivity):
            T = MedsafeLatestRegulatoryActivityDb
        elif isinstance(item, MedsafeIndications):
            T = MedsafeIndicationsDb
        elif isinstance(item, MedsafeMain):
            T = MedsafeMainDb

        self.insert_item(item, T)

