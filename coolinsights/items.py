

from scrapy import Field, Item

class EdqmCEP(Item):

    SubstanceNumber = Field()
    Substance = Field()
    CertificateHolder = Field()
    CertificateNumber = Field()
    IssueDate = Field()
    Status = Field()
    Enddate	= Field()
    Type = Field()

class EdqmReference(Item):

    Availablesince = Field()
    CatNo = Field()
    Name = Field()
    BatchNo = Field()
    UnitQuantity = Field()
    Price = Field()
    SDSProductCode = Field()
    MonographNumber = Field()

class EdqmPractical(Item):
    Tests = Field()
    BrandNameInformation = Field()
    MonographNumber = Field()

class EdqmMain(Item):

    Status = Field()
    MonographNumber = Field()
    EnglishName = Field()
    FrenchName = Field()
    LatinName = Field()
    PinyinName = Field()
    ChineseName = Field()
    Pharmeuropa = Field()
    PublishedinEnglishSupplement = Field()
    PublishedinFrenchSupplement = Field()
    Ongoing = Field()
    Stateofwork = Field()
    Pharmeuropa = Field()
    Description = Field()
    Chromatogram = Field()
    Additionalinformation = Field()
    History = Field()
    InterchangeableICH_Q4B = Field()
    InternationalHarmonisationchapter = Field()

class MedsafeComposition(Item):

    MedID = Field()
    Ingredient = Field()
    Manufacturer = Field()
    Component = Field()

class MedsafeProduction(Item):

    MedID = Field()
    Manufacturer = Field()
    Manufacturingstep = Field()

class MedsafePackaging(Item):

    MedID = Field()
    Contents = Field()
    ShelfLife = Field()
    Package = Field()

class MedsafeLatestRegulatoryActivity(Item):

    MedID = Field()
    PaymentDate = Field()
    Changes = Field()
    Status = Field()
    Priority = Field()
    ApplicationType = Field()
    ApplicationDate = Field()

class MedsafeIndications(Item):

    MedID = Field()
    text = Field()

class MedsafeMain(Item):

    MedID = Field()
    TradeName = Field()
    DoseForm = Field()
    Strength = Field()
    Identifier= Field()
    Sponsor	= Field()
    Applicationdate	= Field()
    Registrationsituation= Field()
    Classification = Field()


