# -*- coding: utf-8 -*-

from furl import furl
from scrapy import Spider, Selector, Request
from scrapy import signals

from collections import OrderedDict

from coolinsights.items import (MedsafeComposition, MedsafeProduction, MedsafePackaging,
                                MedsafeLatestRegulatoryActivity, MedsafeIndications, MedsafeMain)

def clean_key(key):
    key = key.replace('(', '')
    key = key.replace(')', '')
    key = key.replace(' ', '')
    return key


def strip_spaces(raw_str):
    import sys

    def clean_str(s):
        clean = s.strip()
        return clean.replace('\n', ' ').replace('\t','')

    if sys.version_info[0] < 3:
        check_for = (unicode, str)
    else:
        check_for = (str)

    if isinstance(raw_str, check_for):
        return clean_str(raw_str)
    else:
        return raw_str

class MedsafeSpider(Spider):

    name = 'medsafe'
    allowed_domains = ['medsafe.govt.nz']
    start_urls = ['http://www.medsafe.govt.nz']


    def __init__(self, *args, **kwargs):
        self.init = int(kwargs.get('init', 1))
        self.end = int(kwargs.get('end', 4000))

    def start_requests(self):

        for medid in range(self.init, self.end):
            url = 'http://www.medsafe.govt.nz/regulatory/ProductDetail.asp?ID={}'.format(medid)

            yield Request(url,
                          meta={'medid': medid},
                          callback=self.parse_sections)

    def parse(self, response):
        pass

    def parse_row(self, medid, section_type, headers, raw_tr):

        trsel = Selector(text=raw_tr)
        if section_type == 'Composition':
            row = MedsafeComposition()
        elif section_type == 'Production':
            row = MedsafeProduction()
        elif section_type == 'Packaging':
            row = MedsafePackaging()
        elif section_type == 'Latest Regulatory Activity':
            row = MedsafeLatestRegulatoryActivity()
        elif section_type == 'Indications':
            row = MedsafeIndications()

        for i, td in enumerate(trsel.xpath('//td').extract()):
            tdsel = Selector(text=td)
            text = "\n".join([t.replace('\xa0','') for t in tdsel.xpath('//text()').extract()])
            key = clean_key(headers[i])

            row[key] = text

        row['MedID'] = medid

        return row

    def parse_main_section(self, raw_section, medid ):

        def extract_headers(tr_header):

            trhsel = Selector(text=tr_header)
            headers = trhsel.xpath('//td[@bgcolor="silver"]/em/text()').extract()
            return headers

        def extract_values(tr_value):
            trvsel = Selector(text=tr_value)
            values = []
            for td in trvsel.xpath('//td').extract():
                tdsel = Selector(text=td)
                value = "\n".join([t for t in tdsel.xpath('//text()').extract()])
                values.append(value)

            return values


        secsel = Selector(text=raw_section)
        trs = secsel.xpath('//tr').extract()

        h1 = extract_headers(trs[0])
        h2 = extract_headers(trs[2])
        v1 = extract_values(trs[1])
        v2 = extract_values(trs[3])

        main_item = MedsafeMain()

        for i, h in enumerate(h1):
            key = clean_key(h)
            main_item[key] = v1[i]

        for i, h in enumerate(h2):
            key = clean_key(h)
            main_item[key] = v2[i]

        return main_item

    def parse_section(self, raw_section, medid ):

        def extract_headers(tr_header):

            trhsel = Selector(text=tr_header)
            headers = trhsel.xpath('//td[@bgcolor="silver"]/em/text()').extract()
            return headers

        secsel = Selector(text=raw_section)
        section_header = secsel.xpath('//h4/text()').extract_first()
        trs = secsel.xpath('//tr').extract()

        if trs:
            headers = extract_headers(trs[0])
            for tr in trs:
                yield self.parse_row(medid, section_header, headers, tr)

        else:

            if section_header == 'Indications':
                text = secsel.xpath('//p/text()').extract_first()
                indications = MedsafeIndications()
                indications['text'] = text

                yield indications

    def parse_sections(self, response):

        medid = response.meta['medid']

        if b"Couldn't find the product selected" in response.body:
            return
        main_content = response.xpath('//div[@id="content-area" and @class="part-width"]').extract_first()

        if not main_content:
            return
        sections = main_content.split('<h4>')

        for s in sections[1:]:
            raw_section = "<h4>" + s
            for item in self.parse_section(raw_section, medid):
                yield item

        main_section = response.xpath('//div[@id="content-area" and @class="part-width"]//table[@border="0" and @style="width: 100%"]').extract_first()
        yield self.parse_main_section(main_section, medid)



