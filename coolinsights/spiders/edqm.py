# -*- coding: utf-8 -*-

from furl import furl
from scrapy import Spider, Selector, Request

from collections import OrderedDict

from coolinsights.items import EdqmCEP, EdqmReference, EdqmPractical, EdqmMain

def strip_spaces(raw_str):
    import sys

    def clean_str(s):
        clean = s.strip()
        return clean.replace('\n', ' ').replace('\t','')

    if sys.version_info[0] < 3:
        check_for = (unicode, str)
    else:
        check_for = (str)

    if isinstance(raw_str, check_for):
        return clean_str(raw_str)
    else:
        return raw_str

class EdqmSpider(Spider):
    name = 'edqm'
    allowed_domains = ['extranet.edqm.eu']
    start_urls = ['http://extranet.edqm.eu/']


    def __init__(self, *args, **kwargs):
        self.init = int(kwargs.get('init', 1))
        self.end = int(kwargs.get('end', 4000))

    def parse(self, response):

        for i in range(self.init, self.end+1):
            url = 'https://extranet.edqm.eu/4DLink1/4DCGI/Web_View/mono/'+str(i)
            self.log(url)
            yield Request(url,
                          callback=self.parse_mono)

    def _clean_up(self, s):

        return " ".join([strip_spaces(k) for k in s])

    def parse_mono(self, response):

        mono_number = response.url.split('/')[-1]
        response.meta['mono_number'] = mono_number
        for item in self.parse_nested_tables(response):
            yield item

        for item in self.parse_main_table(response):
            yield item

    def get_tables(self, response, table_min_index, table_max_index):

        tables = response.xpath('/descendant::div[@align="left"][2]//table').extract()
        return tables[table_min_index:table_max_index]

    def get_trs(self, response):

        table = response.xpath('/descendant::div[@align="left"][2]//table').extract_first()
        tsel = Selector(text=table)
        for tr in tsel.xpath('//table[@border="0" and @cellpadding="2" and @cellspacing="2"]/tr').extract():
            trsel = Selector(text=tr)

            key = trsel.css('td:first-child b::text, td:first-child font::text').extract()
            value = trsel.css('td:nth-child(2)::text, td:nth-child(2) font::text, td:nth-child(2) a::attr(href)').extract()

            yield tr, key, value

    def get_nested_trs(self, tr, main_header, mono_number):

        def get_headers(tr_header):

            headers = []

            trsel = Selector(text=tr_header)
            for td in trsel.xpath('//td').extract():
                tdsel = Selector(text=td)
                text = tdsel.xpath('//text()').extract()
                header = " ".join([strip_spaces(t) for t in text])
                headers.append(header)

            return headers

        trsel = Selector(text=tr)
        table = trsel.xpath('//table').extract_first()
        tsel = Selector(text=table)
        trs = tsel.xpath('//table//tr').extract()
        all_headers = get_headers(trs[0])
        rows = []
        for tr in trs[1:]:
            trsel = Selector(text=tr)
            tds = trsel.xpath('//td').extract()
            item = self.create_item(all_headers, main_header, tds)

            if main_header != 'CEP':
                item['MonographNumber'] = mono_number

            rows.append(item)

        return rows

    def create_item(self, headers, main_header, tds):

        if main_header == 'CEP':
            item = EdqmCEP()
        elif main_header == 'Practical Information':
            item = EdqmPractical()
        elif main_header == 'Reference standards':
            item = EdqmReference()

        for i, td in enumerate(tds):
            tdsel = Selector(text=td)
            text = tdsel.xpath('//text()').extract()
            value = " ".join([strip_spaces(t) for t in text])

            key = headers[i]
            for r in ('.', '(', ')', '/', ' ', '-'):
                key = key.replace(r, '')

            item[key] = value

        return item

    def parse_main_table(self, response):

        mono_number = response.meta['mono_number']
        normal = OrderedDict()

        main = EdqmMain()
        for full_tr in self.get_trs(response):

            tr, key, value = full_tr

            if len(key) > 1:
                continue

            key = self._clean_up(key)
            value = self._clean_up(value)
            if key == 'Chromatogram':
                main['Chromatogram'] = self.extract_url(response, value)
            elif key == 'History':
                main['History'] = self.extract_url(response, value)
            elif key == '' and value == 'javascript:history.back()':
                continue
            elif key == '' and value == '':
                continue
            elif key == 'Reference standards':
                continue
            elif key == 'CEP':
                continue
            elif key == 'Practical Information':
                continue
            else:
                for r in ('.', '(', ')', '/', ' ', '-'):
                    key = key.replace(r, '')

                main[key] = value

        yield main

    def parse_nested_tables(self, response):

        mono_number = response.meta['mono_number']

        cep = OrderedDict()
        for full_tr in self.get_trs(response):

            tr, key, value = full_tr

            if len(key) <= 1:
                continue
            key = [strip_spaces(k) for k in key]
            if (key[0] == 'Reference standards' and key[1] == 'Available since') or \
                (key[0] == 'Practical Information' and key[1] == 'Test(s)') or \
                (key[0] == 'CEP' and key[1] == 'Substance' and key[2] == 'Number'):

                main_header = key[0]
                key = self._clean_up(key)
                value = self._clean_up(value)
                for nested_tr in self.get_nested_trs(tr, main_header, mono_number):
                    yield nested_tr

    def extract_url(self, response, td):
        tdsel = Selector(text=td)
        return response.urljoin(tdsel.xpath('//a/@href').extract_first())

