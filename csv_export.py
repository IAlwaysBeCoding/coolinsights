
import csv
from scrapy.utils.project import get_project_settings

from sqlalchemy import Column, ForeignKey, Sequence, Integer, \
                        String, Text, Boolean, DateTime, create_engine


from sqlalchemy.orm import scoped_session, sessionmaker


SETTINGS = get_project_settings()

DATABASE = 'mysql+pymysql://{}:{}@{}:{}/{}'.format(
        SETTINGS['MYSQL_USERNAME'], SETTINGS['MYSQL_PASSWORD'],
        SETTINGS['MYSQL_HOST'], SETTINGS['MYSQL_PORT'] , SETTINGS['MYSQL_DATABASE'])


DBSession = scoped_session(sessionmaker())
engine = create_engine(DATABASE, convert_unicode=True, echo=False)
DBSession.configure(bind=engine, autoflush=False, expire_on_commit=False)


def export_to_csv(result, filename, fields):

    filename = open(filename, 'w+')
    writer = csv.DictWriter(filename, delimiter='|', fieldnames=fields)
    writer.writeheader()
    for r in result:
        row = dict([(fields[i], x) for i,x in enumerate(r)])
        writer.writerow(row)

def export_EdqmCEPDb(filename):
    result = DBSession.execute("Select * FROM EdqmCEP")

    fields = [
        'Id',
        'SubstanceNumber',
        'Substance',
        'CertificateHolder',
        'CertificateNumber',
        'IssueDate',
        'Status',
        'Enddate',
        'Type'
    ]
    export_to_csv(result, filename, fields)
    print('Exported table EdqmCEPDb to filename:{}'.format(filename))

def export_EdqmReferenceDb(filename):
    result = DBSession.execute("Select * FROM EdqmReference")

    fields = [
        'Id',
        'Availablesince',
        'CatNo',
        'Name',
        'BatchNo',
        'UnitQuantity',
        'Price',
        'SDSProductCode',
        'MonographNumber'
    ]
    export_to_csv(result, filename, fields)
    print('Exported table EdqmReferenceDb to filename:{}'.format(filename))
def export_EdqmPracticalDb(filename):
    result = DBSession.execute("Select * FROM EdqmPractical")

    fields = [
        'Id',
        'Tests',
        'BrandNameInformation',
        'MonographNumber',
    ]
    export_to_csv(result, filename, fields)
    print('Exported table EdqmPracticalDb to filename:{}'.format(filename))
def export_EdqmMainDb(filename):
    result = DBSession.execute("Select * FROM EdqmMain")

    fields = [
        'Id',
        'Status',
        'MonographNumber',
        'EnglishName',
        'FrenchName',
        'LatinName',
        'PinyinName',
        'ChineseName',
        'Pharmeuropa',
        'PublishedinEnglishSupplement',
        'PublishedinFrenchSupplement',
        'Ongoing',
        'Stateofwork',
        'Pharmeuropa',
        'Description',
        'Chromatogram',
        'Additionalinformation',
        'History',
        'InterchangeableICH_Q4B',
        'InternationalHarmonisationchapter',
    ]
    export_to_csv(result, filename, fields)
    print('Exported table EdqmMainDb to filename:{}'.format(filename))
def export_MedsafeCompositionDb(filename):
    result = DBSession.execute("Select * FROM MedsafeComposition")

    fields = [
        'Id',
        'MedID',
        'Ingredient',
        'Manufacturer',
        'Component',
    ]
    export_to_csv(result, filename, fields)
    print('Exported table MedsafeCompositionDb to filename:{}'.format(filename))
def export_MedsafeProductionDb(filename):
    result = DBSession.execute("Select * FROM MedsafeProduction")

    fields = [
        'Id',
        'MedID',
        'Manufacturer',
        'Manufacturingstep',
    ]
    export_to_csv(result, filename, fields)
    print('Exported table MedsafeProductionDb to filename:{}'.format(filename))
def export_MedsafePackagingDb(filename):
    result = DBSession.execute("Select * FROM MedsafePackaging")

    fields = [
        'Id',
        'MedID',
        'Contents',
        'ShelfLife',
        'Package',
    ]
    export_to_csv(result, filename, fields)
    print('Exported table MedsafePackagingDb to filename:{}'.format(filename))
def export_MedsafeLatestRegulatoryActivityDb(filename):
    result = DBSession.execute("Select * FROM MedsafeLatestRegulatoryActivity")

    fields = [
        'Id',
        'MedID',
        'PaymentDate',
        'Changes',
        'Status',
        'Priority',
        'ApplicationType',
        'ApplicationDate',
    ]
    export_to_csv(result, filename, fields)
    print('Exported table MedsafeLatestRegulatoryActivityDb to filename:{}'.format(filename))
def export_MedsafeIndicationsDb(filename):
    result = DBSession.execute("Select * FROM MedsafeIndications")

    fields = [
        'Id',
        'MedID',
        'text',
    ]
    export_to_csv(result, filename, fields)
    print('Exported table MedsafeIndicationsDb to filename:{}'.format(filename))
def export_MedsafeMainDb(filename):
    result = DBSession.execute("Select * FROM MedsafeMain")

    fields = [
        'Id',
        'MedID',
        'TradeName',
        'DoseForm',
        'Strength',
        'Identifie',
        'Sponsor',
        'Applicationdate',
        'Registrationsituatio',
        'Classification',
    ]
    export_to_csv(result, filename, fields)
    print('Exported table MedsafeMainDb to filename:{}'.format(filename))


if __name__ == '__main__':

    export_MedsafeMainDb('MedsafeMainDb.csv')
    export_MedsafeIndicationsDb('MedsafeIndicationsDb.csv')
    export_MedsafeLatestRegulatoryActivityDb('MedsafeLatestRegulatoryActivityDb.csv')
    export_MedsafePackagingDb('MedsafePackagingDb.csv')
    export_MedsafeProductionDb('MedsafeProductionDb.csv')
    export_MedsafeCompositionDb('MedsafeCompositionDb.csv')
    export_EdqmCEPDb('EdqmCEPDb.csv')
    export_EdqmMainDb('EdqmMainDb.csv')
    export_EdqmPracticalDb('EdqmPracticalDb.csv')
    export_EdqmReferenceDb('EdqmReferenceDb.csv')
